Листинг 4.1: Макет сайта учебного приложения. app/views/layouts/application.html.erb

<!DOCTYPE html>
<html>
  <head>
    <title><%= yield(:title) %> | Ruby on Rails Tutorial Sample App</title>
    <%= stylesheet_link_tag    'application', media: 'all',
                                              'data-turbolinks-track' => true %>
    <%= javascript_include_tag 'application', 'data-turbolinks-track' => true %>
    <%= csrf_meta_tags %>
  </head>
  <body>
    <%= yield %>
  </body>
</html>
----------------------
git push origin yield
не работает <!--#Листинг 5.4: Добавление Bootstrap CSS. app/assets/stylesheets/custom.css.scss
#@import "bootstrap-sprockets";
-->
Листинг 5.8: Макет сайта с партиалами для таблицы стилей и заголовка. app/views/layouts/application.html.erb

<!DOCTYPE html>
<html>
  <head>
    <title><%= full_title(yield(:title)) %></title>
    <%= stylesheet_link_tag 'application', media: 'all',
                                           'data-turbolinks-track' => true %>
    <%= javascript_include_tag 'application', 'data-turbolinks-track' => true %>
    <%= csrf_meta_tags %>
    <%= render 'layouts/shim' %>
  </head>
  <body>
    <%= render 'layouts/header' %>
    <div class="container">
      <%= yield %>
    </div>
  </body>
</html>
----------
Листинг 5.28: Создание контроллера Users (с действием new).

$ rails generate controller Users new
